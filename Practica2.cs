﻿using System;

namespace Ejercicio1
{

    class Program
    {

        private int[] vect;

        public Program()
        {
            Console.Write("Tamaño del Vector: ");
            int a = int.Parse(Console.ReadLine());
            vect = new int[a];
            Console.WriteLine();
        }

        public void Cargar()
        {
            for (int i = 0; i < vect.Length; i++)
            {
                Console.Write("Ingresar un Valor: ");
                vect[i] = int.Parse(Console.ReadLine());
            }
        }

        public void MenorMayor(out int ma, out int me)
        {
            ma = vect[0];
            me = vect[0];

            for (int i = 1; i < vect.Length; i++)
            {
                if (vect[i] > ma)
                {
                    ma = vect[i];
                }
                else
                {
                    if (vect[i] < me)
                    {
                        me = vect[i];
                    }
                }

            }
        }

        static void Main(string[] args)
        {
            Program p = new Program();

            p.Cargar();

            int mayor, menor;

            p.MenorMayor(out mayor, out menor);
            Console.WriteLine("El mayor elemento de la tabla es: {0}", mayor);
            Console.WriteLine("El menor elemento de la tabla es: {0}", menor);
        }
    }
}